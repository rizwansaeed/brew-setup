#!/bin/bash -e

usage()
{
	echo "Usage: $0 [-i|-r] [-f -c -a]"
  	exit 1
}

while getopts 'irfca' opt
do
  case $opt in
    i) action="install" ;;
    r) action="reinstall" ;;
	f) type_formulae="y" ;;
	c) type_cask="y" ;;
	a) type_appstore="y" ;;
  esac
done

# check an action has been given
[ -z "$action" ] && usage

# mas uses 'upgrade' rather than 'reinstall'
if [ "$action" == "reinstall" ]; then
	mas_action="upgrade"
else
	mas_action=$action
fi

install_formulae()
{
	echo "### $action formuale ###"

	declare -a cli_apps=(
		'awscli'
		'cheat'
		'doctrl'
		'fastlane'
		'git'
		'helm'
		'htop'
		'jq'
		'kubernetes-cli'
		'mas'
		'nmap'
		'node'
		'nmap'
		'poetry'
		'pyenv'
		'python'		
		'rbenv'
	)

	for app in "${cli_apps[@]}"; do
		brew $action "$app"		
	done  
}

install_casks()
{
	echo "### $action casks ###"

	declare -a cask_apps=(
		'1password'
		'alfred'
		'appcleaner'
		'backblaze'
		'boop'
		'docker'
		'dotnet-sdk'
		'drawio'
		'gitkraken'
		'google-chrome'
		'intellij-idea'
		'iterm2'
		'jetbrains-toolbox'
		'lastpass'
		'lens'		
		'ngrok'
		'notion'
		'postman'
		'ring'
		'sf-symbols'
		'slack'
		'spotify'
		'sublime-text'
		'temurin'
		'todoist'
		'vlc'
		'vmware-fusion'
		'whatsapp'
		'visual-studio-code'
	)

	for app in "${cask_apps[@]}"; do
		brew cask $action "$app"
	done
}

install_appstore()
{
	#
	#  use https://github.com/mas-cli/mas to get id
	# 

	echo "### $action AppStore ###"

	declare -a mas_apps=(
		'937984704'		# Amphetamine 
		'1091189122'	# Bear
		'411643860'		# DaisyDisk
		'412759995'		# Guidance
		'441258766'		# Magnet
		'1477385213'	# Save to Pocket
		'1090940630'	# TaskPaper
		'425424353'		# The Unarchiver
	)

	for app in "${mas_apps[@]}"; do
		mas $mas_action "$app"
	done
}

if [ -z "$type_formulae" ] && [ -z "$type_cask" ] && [ -z "$type_appstore" ]; then
	install_formulae
	install_casks
	install_appstore
else  
	[ ! -z "$type_formulae" ] && install_formulae
	[ ! -z "$type_cask" ] && install_casks
	[ ! -z "$type_appstore" ] && install_appstore
fi